package TestAutomat;

import java.util.ArrayList;

public class TransitionList
{
    private ArrayList<Transition> transitionList = new ArrayList<Transition>();
    
    public void addTransition(Transition tr)
    {
        transitionList.add(tr);
    }
    
    Transition getTransition(int index)
    {
        return transitionList.get(index);
    }
    
    Transition findTransition(String initialState, char symbol)
    {
        for(Transition tr: transitionList)
            if(tr.getInitialState().equals(initialState) && tr.getSymbol() == symbol)
               return tr;
        return null;
    }
    
    @Override
    public String toString()
    {
        String print = "";
        for(Transition tr: transitionList)
            print += "d(" + tr.getInitialState() + "," + tr.getSymbol() + ")=" + tr.getFinalState() + "\n";
        return print;
    }
}
