package TestAutomat;

import java.io.*;
import java.util.*;

public class Automat 
{
    private String initialState, finalStatesLine, transitionLine;
    private TransitionList list;
    private ArrayList<String> f;
    private String[] finalState, transitionItem;
    
    ///List<Transition> delta;
    BufferedReader buf;
   // int k=0;
    
    public Automat(String file_name) throws FileNotFoundException, IOException
    {
        buf = new BufferedReader(new FileReader("automat.txt"));
        list = new TransitionList();
        f = new ArrayList<>();
        
        initialState = buf.readLine(); 
        
        finalStatesLine = buf.readLine();
        finalState = finalStatesLine.split(" ");       
        for(int i=0; i<finalState.length; i++)
            f.add(finalState[i]);

        while(true)
        {
            transitionLine = buf.readLine();
            //citim din fisier o tranzitie
            if(transitionLine != null)
            {
               
                transitionItem = transitionLine.split(" ");
                //o memoram intr-un vector separat
                
		Transition tr=new Transition(transitionItem[0],transitionItem[1].charAt(0),transitionItem[2]);
                //cream un obiect tr de tip Transition
		list.addTransition(tr);
                //il adaugam in lista de tip Tranition List
            }
            else
                break;
        }          
    }
    
    public void printAutomat()
    {
        System.out.println("Stare initiala: "+ initialState);
	System.out.print("Stari finale: ");
	for(String finalState: f)
            System.out.print(finalState + " ");
	System.out.println(" ");
    	System.out.println(list);
    }
    
    public void analyzeWord(String word, String currentState)
    {
        Transition currentTransition = null;
        //conditia de oprire din recursivitate
        if("".equals(word))
            //daca cuvantul nu mai are litere
            if(f.contains(currentState))
            {   
                //si starea curenta la care se afla face parte din multimea starilor finale
                System.out.println("Cuvantul apartine.");
                return;
            }
            else
                {   
                    System.out.println("Cuvantul nu apartine.");
                    return;
                }
                // gasim tranzitia pentru prima litera
            currentTransition = list.findTransition(currentState, word.charAt(0));
            if(currentTransition != null)
            {
                word = word.substring(1);
                analyzeWord(word, currentTransition.getFinalState());
            }
            else
                {   
                    System.out.println("Cuvantul nu apartine.");
                    return;
                }
    }
    
    public String getInitialState()
    {
        return initialState;
    }
}
