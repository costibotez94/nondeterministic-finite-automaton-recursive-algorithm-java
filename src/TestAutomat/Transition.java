package TestAutomat;

public class Transition 
{
    private String initialState, finalState;
    private char symbol; 
    
    //this.initialState reprezinta o variabile din clasa care se initializeaza cu paramentrul. Sa nu se faca
    //confuzie de nume. SUNT DIFERITE!
    public Transition(String initialState, char symbol, String finalState)
    {
        this.initialState = initialState;
        this.symbol = symbol;
        this.finalState = finalState;
        
    }
    
    //functii care intorc proprietatile clasei
    public String getInitialState()
    {
        return initialState;
    }
    
    public char getSymbol()
    {
        return symbol;
    }
    
    public String getFinalState()
    {
        return finalState;
    }
}
